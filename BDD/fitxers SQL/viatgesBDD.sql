DROP TABLE IF EXISTS `a18josibacuc_ProjecteViatges`.`categories`;

CREATE DATABASE `a18josibacuc_ProjecteViatges`;

DROP TABLE IF EXISTS `a18josibacuc_ProjecteViatges`.`categories`;
CREATE TABLE `a18josibacuc_ProjecteViatges`.`categories` (
	`codi` int(6) AUTO_INCREMENT PRIMARY KEY,
	`nom` varchar(35)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `a18josibacuc_ProjecteViatges`.`usuaris`;
CREATE TABLE `a18josibacuc_ProjecteViatges`.`usuaris` (
	`alias` varchar(128) PRIMARY KEY,
	`password` varchar(50),
	`data_registre` date,
	`reputacio` varchar(150)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `a18josibacuc_ProjecteViatges`.`experiencies`;
CREATE TABLE `a18josibacuc_ProjecteViatges`.`experiencies` (
	`codi` int(6) AUTO_INCREMENT PRIMARY KEY,
	`titol` varchar(128),
	`data` date NOT NULL,
	`text` varchar(1500),
	`imatge` varchar(150),
    `coordenades` varchar(35),
    `num_valoracions` int DEFAULT 0,
    `estat` VARCHAR (20),
	`codi_categoria` int (6),
    `alias_usuari` VARCHAR(128),
    FOREIGN KEY (`alias_usuari`) REFERENCES `a18josibacuc_ProjecteViatges`.`usuaris`(`alias`) ON DELETE SET NULL ON UPDATE CASCADE,
	FOREIGN KEY (`codi_categoria`) REFERENCES `a18josibacuc_ProjecteViatges`.`categories`(`codi`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
