<?php
   header("Content-Type: text/html;charset=utf-8");

   require "viatges_bdd.php";
   require "configuracio_bdd.php";

   if (!isset($_POST['codiExp'])) {
      die ('Error!');
   }

   $bdd = new ViatgesBDD($db_host, $db_user, $db_pass, $db_name);
   $bdd->punctuarNegExperienciesByCodi($_POST['codiExp']);
?>