<?php
   header("Content-Type: text/html;charset=utf-8");

   require "viatges_bdd.php";
   require "configuracio_bdd.php";

   if (!isset($_POST['userEd'], $_POST['passwordEd'])) {
      die ('Escriu l\'alias i la clau que vols.');
   }

   $bdd = new ViatgesBDD($db_host, $db_user, $db_pass, $db_name);
   $bdd->updateAliasAndPassword($_POST['userEd'], $_POST['passwordEd'], $_POST['anticUser']);
?>