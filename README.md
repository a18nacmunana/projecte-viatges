# Projecte Viatges

## Integrants

* Nacho Munné
* Oscar Ortega
* José Carlos Ibarra

## Objectiu

El projecte té com a objectiu la creació d'una SPA (aplicació web interactiva, sense recàrrega) 
per un negoci d'experiències.

## Estat breu del projecte (actualitzat)

- 9/12: Començem! Escribim el esbós inicial de les tasques i subtasques en el Jira. Repartim les substasques 
entre els integrants del grup.Redactem el readme.
- 10/12: Hem afegit un modal a la pàgina inicial. A la part del servidor, hem acabat la classe on hi ha els 
mètodes d'introduir usuari, modificar-lo, recuperar-lo, introduir experiències, puntuar. A més hem agregat un 
arxiu anomenat .gitignore. per a que no es puji al git i només ho veiem nosaltres. Ens adeqüem als terminis 
de l'sprint. 
- 11/12: Final primer sprint, agrupacio del backend (base de dades) amb el frontend (index.php) 
per poder crear experiencies correctament i es guardin a la base de dades. Trucada de axios al 
php per recollir les dades que calen.
- 12/12: Finalització de les experiencies per a que apareguin correctament i funcioni junt a la base de dades.
- 13/12: Retocs al login i optimització del index, vam agregar la imatge animada, el registre i canvi de logo.
A més, hem posat els botons de les experiencies per poder llistar-les i editar-les.
- 16/12: Retocs al footer, canvi de logotip, agrupar el funcionament de la base de dades a les diferents
funcionalitats que demana el projecte(llistar experiencies, ordenar-les...)
- 17/12: Finalització de la funcio de vots i filtrar categories. 
- 18/12: Funcio editar experiencies i afegit un modal de la informacio adicional a les experiencies. També una funció
per poder esborrar experiencies (només les propies, es a dir, que ha pujat l'usuari que esta logeat en aquell moment).
- 19/12: Funció administrador, desplegament final al labs i ultimes modificacions per enllestir el projecte.
 
## Adreça del JIRA

[Link al nostre JIRA](http://labs.iam.cat:8080/projects/VIAT2/summary)

## Adreça web del projecte desplegat

(Adreça al labs.iam.cat)

http://labs.iam.cat/~a18josibacuc/projecte-viatges/
