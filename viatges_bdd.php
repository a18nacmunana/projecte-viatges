<?php
    class ViatgesBDD {
        static private $db_host;
        static private $db_user;
        static private $db_pass;
        static private $db_name;

        protected $query;
        public $rows = array();

        private $conn;

        // Proporciona configuración de MySQL
        function __construct($db_host, $db_user, $db_pass, $db_name) {
            self::$db_host = $db_host;
            self::$db_user = $db_user;
            self::$db_pass = $db_pass;
            self::$db_name = $db_name;
        }

        private function open_connection() {
            $this->conn = new mysqli(self::$db_host, self::$db_user, self::$db_pass, self::$db_name);

            if($this->conn->connect_error) {
                die("La conexión con la BD ha fallado. <br>Error: ".$this->conn->connect_error);
            }

            mysqli_set_charset($this->conn,'utf8'); 
        }

        private function close_connection() {
            $this->conn->close();
        }

        private function execute_single_query() {
            $this->open_connection();
            if($this->conn->query($this->query)) {
                echo "¡Operación realizada correctamente!";
            } else {
                echo "Error!";
            }
            $this->close_connection();
        }

        private function get_results_from_query() {
            $this->open_connection();
            $result = $this->conn->query($this->query);

            for($i = 0; $i < $result->num_rows; $i++) {
                $this->rows[$i] = $result->fetch_assoc();
                foreach ($this->rows[$i] as $key => $value) {
                    echo "$key: $value<br>";
                }
            }

            $result->close();
            $this->close_connection();
        }

        private function get_single_result_from_query() {
            $this->open_connection();
            $result = $this->conn->query($this->query);

            for($i = 0; $i < $result->num_rows; $i++) {
                $this->rows[$i] = $result->fetch_assoc();
                foreach ($this->rows[$i] as $key => $value) {
                    echo $value;
                }
            }

            $result->close();
            $this->close_connection();
        }

        private function get_results() {
            $this->open_connection();
            $result = $this->conn->query($this->query);

            for($i = 0; $i < $result->num_rows; $i++) {
                $this->rows[$i] = $result->fetch_assoc();
            }

            $result->close();
            $this->close_connection();
        }

        private function loginValidation($alias) {
            $this->open_connection();
            $result = $this->conn->query($this->query);
            
            if($result->num_rows > 0) {
                session_start(); 
                $_SESSION['user_id'] = $alias;
                return "Ok";
            } else {
                return "Error";
            }

            $result->close();
            $this->close_connection();
        }

        private function get_last_results_from_query() {
            $this->open_connection();
            $result = $this->conn->query($this->query);

            for($i = 0; $i < $result->num_rows; $i++) {
                $this->rows[$i] = $result->fetch_assoc();
            }

            $result->close();
            $this->close_connection();
        }

        private function get_single_result__from_query() {
            $this->open_connection();
            $result = $this->conn->query($this->query);

            echo $result->fetch_assoc();

            $result->close();
            $this->close_connection();
        }

        private function update_results_from_query($alias) {
            $this->open_connection();
            if($this->conn->query($this->query)) {
                $_SESSION['user_id'] = $alias;
                echo "¡Operación realizada correctamente!";
            } else {
                echo "Error!";
            }
            $this->close_connection();
        }

        private function update_results_from_queryx() {
            $this->open_connection();
            if($this->conn->query($this->query)) {
                echo "¡Operació realitzada correctament!";
            } else {
                echo "Error!";
            }
            $this->close_connection();
        }

        private function rate_from_query() {
            $this->open_connection();
            if($this->conn->query($this->query)) {
                echo "¡Operación realizada correctamente!";
            } else {
                echo "Error!";
            }
            $this->close_connection();
        }

        function insertUsuari($alias, $password, $reputacio) {
            $date = date('Y-m-d');
            if($alias != "" && $password != "" && $reputacio != "") {
                $this->query = "INSERT INTO a18josibacuc_ProjecteViatges.usuaris (alias, password, data_registre, reputacio) VALUES ('$alias', '$password', '$date', '$reputacio');";
                $this->execute_single_query(); 
            }
        }

        function getTotsUsuaris() {
            $this->query = "SELECT * FROM a18josibacuc_ProjecteViatges.usuaris;";
            $this->get_results_from_query();
        }


        function getUsuariByAliasAndPassword($alias, $password) {
            $this->query = "SELECT * FROM a18josibacuc_ProjecteViatges.usuaris WHERE alias='$alias' AND password='$password';";
            return $this->loginValidation($alias);
        }

        function getUsuarisByCodi($codi) {
            $this->query = "SELECT * FROM a18josibacuc_ProjecteViatges.usuaris WHERE codi='"."$codi"."'";
            $this->get_results();

            return $this->rows;
        }

        function getUsuarisByNom($alias) {
            $this->query = "SELECT * FROM a18josibacuc_ProjecteViatges.usuaris WHERE alias='"."$alias"."'";
            $this->get_results();

            return $this->rows;
        }

        function deleteUsuariByAlias($alias) {
            if($alias != "") {
                $this->query = "DELETE FROM usuaris WHERE alias='".$alias."';";
                $this->update_results_from_query();
            }
        }

        function deleteUsuariByAliasx($alias) {
            if($alias != "") {
                $this->query = "DELETE FROM usuaris WHERE alias='".$alias."';";
                $this->update_results_from_queryx();
            }
        }

        function updateAlias($alias) {
            if($alias != "") {
                $this->query = "UPDATE usuaris SET alias='$alias' WHERE alias='$alias';";
                $this->update_results_from_query();
            }
        }

        function updateAliasAndPassword($alias, $password, $anticUser) {
            if($alias != "" && $password != "") {
                $this->query = "UPDATE usuaris SET alias='$alias', password='$password' WHERE alias='$anticUser';";
                $this->update_results_from_query($alias);
            }
        }

        function updateTitolExperiencia($titol, $codi){
            $this->query = "UPDATE a18josibacuc_ProjecteViatges.experiencies SET titol='$titol' WHERE codi='$codi';";
            $this->execute_single_query(); 
        }

        function updateImatgeExperiencia($imatge, $codi){
            $this->query = "UPDATE a18josibacuc_ProjecteViatges.experiencies SET imatge='$imatge' WHERE codi='$codi';";
            $this->execute_single_query(); 
        }

        function updateDescripcioExperiencia($text, $codi){
            $this->query = "UPDATE a18josibacuc_ProjecteViatges.experiencies SET text='$text' WHERE codi='$codi';";
            $this->execute_single_query(); 
        }

        function getTotesCategories() {
            $this->query = "SELECT * FROM a18josibacuc_ProjecteViatges.categories";
            $this->get_results_from_query();
        }

        function getTotesExperiencies() {
            $this->query = "SELECT * FROM a18josibacuc_ProjecteViatges.experiencies";
            $this->get_results();

            return $this->rows;
        }

        function getDarreresExperiencies() {
            $this->query = "SELECT * FROM a18josibacuc_ProjecteViatges.experiencies ORDER BY codi DESC LIMIT 9;";
            $this->get_last_results_from_query();

            return $this->rows;
        }

        function getExperienciesByCategoria($codi_categoria) {
            $this->query = "SELECT * FROM a18josibacuc_ProjecteViatges.experiencies WHERE codi_categoria = $codi_categoria;";
            $this->get_results();

            return $this->rows;
        }

        function insertExperiencia($titol, $text, $image, $coordenades, $estat, $codi_categoria, $alias_usuari) {
            $date = date('Y-m-d');
            if($titol != "" && $text != "" && $image != "" && $coordenades != "" && $estat != "" && $codi_categoria != "") {
                $this->query = "INSERT INTO a18josibacuc_ProjecteViatges.experiencies (titol, data, text, imatge, coordenades, estat, codi_categoria, alias_usuari) VALUES ('$titol', '$date','$text', '$image', '$coordenades', '$estat', '$codi_categoria', '$alias_usuari');";
                $this->execute_single_query(); 
            }
        }

        function insertCategoria($nom){
            $this->query = "INSERT INTO a18josibacuc_ProjecteViatges.categories (nom) VALUES ('$nom');";
            $this->execute_single_query();
        }

        function getCodiCategoriaByName($name) {
            $this->query = "SELECT codi FROM a18josibacuc_ProjecteViatges.categories WHERE nom='$name';";
            $this->get_single_result_from_query();
        }

        function getExperienciesByCodi($codi) {
            $this->query = "SELECT * FROM a18josibacuc_ProjecteViatges.experiencies WHERE codi='"."$codi"."'";
            $this->get_results_from_query();
        }

        function getExperienciesByAliasUsuari($alias) {
            $this->query = "SELECT * FROM a18josibacuc_ProjecteViatges.experiencies WHERE alias_usuari='$alias';";
            $this->get_results();

            return $this->rows;
        }

        function punctuarPosExperienciesByCodi($codi) {
            $this->query = "UPDATE experiencies
            SET num_valoracions_pos = num_valoracions_pos + 1
            WHERE codi = '$codi'";

            $this->rate_from_query();
        }

        function punctuarNegExperienciesByCodi($codi) {
            $this->query = "UPDATE experiencies
            SET num_valoracions_neg = num_valoracions_neg + 1
            WHERE codi = '$codi'";

            $this->rate_from_query();
        }

        function getCodiExperienciesByName($name) {
            $this->query = "SELECT codi FROM a18josibacuc_ProjecteViatges.categories WHERE nom='"."$name"."'";
            $this->get_results();
            
            return $this->rows;
        }

        function deleteExperienciaByCodiAndAlias($codi, $alias) {
            if($codi != "") {
                $this->query = "DELETE FROM a18josibacuc_ProjecteViatges.experiencies WHERE codi='$codi' AND alias_usuari='$alias';";
                $this->execute_single_query(); 
            }
        }

        function getTipusUsuari($name){
            $this->query = "SELECT reputacio FROM a18josibacuc_ProjecteViatges.usuaris WHERE alias='$name';";
            $this->get_results();

            return $this->rows;
        }
    }
?>
