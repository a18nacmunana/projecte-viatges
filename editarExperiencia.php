<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: text/html; charset=utf-8");

    require "viatges_bdd.php";
    require "configuracio_bdd.php";

    $bdd = new ViatgesBDD($db_host, $db_user, $db_pass, $db_name);

    if (isset($_POST['titolExp'])) {
        $bdd->updateTitolExperiencia($_POST['titolExp'], $_POST['codiExp']);
    } else if (isset($_POST['descripcioExp'])) {
        $bdd->updateDescripcioExperiencia($_POST['descripcioExp'], $_POST['codiExp']);
    } else if (isset($_POST['imatgeExp'])) {
        $bdd->updateImatgeExperiencia($_POST['imatgeExp'], $_POST['codiExp']);
    }

?>