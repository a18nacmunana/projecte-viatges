document.getElementsByClassName("enviar")[1].addEventListener('click', function() {
  registre();
});

function registre() {
  let user = document.getElementsByClassName('usuario')[1].value;
  let password = document.getElementsByClassName('password')[1].value;

  let params = new URLSearchParams();
  params.append('userReg', user);
  params.append('passwordReg', password);

  axios.post('registre.php', params).then(function(response) {
    if(response.data == "¡Operación realizada correctamente!") {
        Swal.fire({
            icon: 'success',
            title: 'Registre correcte!'
        })
      } else {
        Swal.fire({
            icon: 'error',
            title: 'Aquest usuari ja existeix!'
        })
      }
  });
}