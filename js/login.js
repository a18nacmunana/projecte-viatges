document.getElementsByClassName("enviar")[0].addEventListener('click', function() {
    login();
});

function login() {
  let user = document.getElementsByClassName('usuario')[0].value;
  let password = document.getElementsByClassName('password')[0].value;
  let modal = document.getElementById("loginModal");

  let params = new URLSearchParams();
  params.append('userLog', user);
  params.append('passwordLog', password);

  axios.post('login.php', params).then(function(response) {
    console.log(response.data);
    if(response.data.estat == "Error") {
      Swal.fire({
        icon: 'error',
        title: 'Credencials incorrectes',
      })
    } else {
      Swal.fire({
        icon: 'success',
        title: 'Has iniciat sessió!',
      })
      if(response.data.tipus == "admin"){
        adm.style.visibility="visible";
        adm.setAttribute('type','button');
        adm.setAttribute('class','btn btn-outline-dark btn-lg btn-block');
        adm.setAttribute('data-toggle', "modal");
        adm.setAttribute('data-target', "#adminModal");
        adm.innerHTML="Panel Administrador";
        console.log("ES ADMIN");
      }
      else{
        adm.style.visibility="hidden";
      }
      document.getElementById("darreres_exp").style.display = "none";
      document.getElementById("pag_personal").style.display = "block";
      
      document.getElementById("usuari_personal").innerHTML = user;
      document.getElementsByClassName("display-10")[0].innerHTML = user + ", gaudeix d'unes bones vacançes!"
    }
  })
  .catch(function (error) {
    console.log(error.response);
  });
}