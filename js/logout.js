document.getElementById("tancar_sessio").addEventListener('click', function() {
    tancarSessio();
})

function tancarSessio() {
    axios.post('logout.php').then(function(response) {
        if(response.data == "ok") {
            Swal.fire({
                icon: 'success',
                title: 'Sessió tancada!',
              })
            document.getElementById("darreres_exp").style.display = "block";
            document.getElementById("pag_personal").style.display = "none";
            document.getElementsByClassName("display-10")[0].innerHTML = "Gaudeix d'unes bones vacançes!"
        }
    });
}
