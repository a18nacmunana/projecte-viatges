document.getElementById("enviar_editar").addEventListener('click', function(){
    editarDades();
});

function editarDades() {
    let anticUser = document.getElementById('usuari_personal').innerHTML;
    let nouUser = document.getElementsByName('nou_alias')[0].value;
    let nouPassword = document.getElementsByName('nou_password')[0].value;

    let params = new URLSearchParams();
    params.append('userEd', nouUser);
    params.append('passwordEd', nouPassword);
    params.append('anticUser', anticUser);

    axios.post('editar_dades.php', params).then(function(response) {
        if(response.data == "¡Operación realizada correctamente!") {
            Swal.fire({
                icon: 'success',
                title: 'Perfil actualitzat!'
            })

            document.getElementById("usuari_personal").innerHTML = nouUser;
            document.getElementsByClassName("display-10")[0].innerHTML = nouUser + ", gaudeix d'unes bones vacançes!";
          } else {
            Swal.fire({
                icon: 'error',
                title: 'Completa tots els camps!'
            })
          }
      });
}