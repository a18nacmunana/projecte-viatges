document.getElementById("btnCat").addEventListener('click', function() {
    afegirExperiencia();
});

function afegirExperiencia() {
    let novaCategoria = document.getElementsByName('novaCat')[0].value;

    let params = new URLSearchParams();
    params.append('novaCategoria', novaCategoria);
  
    axios.post('afegirCategoria.php', params).then(function(response) {
      console.log(response.data);
      if(response.data == "¡Operación realizada correctamente!") {
        Swal.fire({
          icon: 'success',
          title: 'Categoria afegida',
        })
      } else {
        Swal.fire({
          icon: 'error',
          title: 'ERROR',
        })
      }
    })
    .catch(function (error) {
      console.log(error.response);
    });
  }