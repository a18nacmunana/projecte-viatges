axios({
        method: 'post',
        url: 'totesExperiencies.php',
        responseType: 'json'
    })
    .then(function (response) {
        console.log(response.data);
        let experiencies= document.getElementById("targetesExp");
        let num_experiencies= response.data.length;
        response.data.reverse();
        for (let index = 0; index < num_experiencies; index++) {
            experiencies.innerHTML +=
            `
        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
          <img src="${response.data[index].imatge}" class="bd-placeholder-img card-img-top" width="100%" height="225"</img>
          <div class="card-body">
              <p class="card-text">
                <h3>${response.data[index].titol}</h3>
              </p>
              <p>Publicat per: ${response.data[index].alias_usuari}</p>
              
              <div id="textModal${response.data[index].codi}" class="modal fade" role="dialog">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                    <h4 class="modal-title">Descripció</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                      ${response.data[index].text}
                    </div>
                  </div>
                </div>
              </div>
              
              <small class="text-muted">Data de publicació: ${response.data[index].data}</small>
              <div class="d-flex justify-content-between align-items-center">
              <div class="btn-group btn-group-sm mt-2 float-right" role="group"">
                <button type="button" class="btn btn-outline-dark btn-sm" data-toggle="modal" data-target="#textModal${response.data[index].codi}">Descripció</button>
                <button id="Pos${response.data[index].codi}" onclick="punctuarPosExp(${response.data[index].codi})" type="button" class=" btn btn-outline-success">${response.data[index].num_valoracions_pos}</button>
                <button id="Neg${response.data[index].codi}" onclick="punctuarNegExp(${response.data[index].codi})" type="button" class=" btn btn-outline-danger">${response.data[index].num_valoracions_neg}</button>
              </div>
              </div>
          </div>
          </div>
        </div>
        `
        }
    });