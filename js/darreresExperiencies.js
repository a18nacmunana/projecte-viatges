axios({
  method: 'post',
  url: 'darreresExperiencies.php',
  responseType: 'json'
})
.then(function (response) {
  console.log(response.data)
  let experiencies = document.getElementById("experiencies");
  let num_experiencies= response.data.length;

  for (let index = 0; index < num_experiencies; index++) {
      experiencies.innerHTML += 
      `
      <div class="col-md-4">
      <div class="card mb-4 shadow-sm">
        <img src="${response.data[index].imatge}" class="bd-placeholder-img card-img-top" width="100%" height="225"</img>
        <div class="card-body">
          <p class="card-text">${response.data[index].titol}</p>
          <div class="d-flex justify-content-between align-items-center">
            <small class="text-muted">Data de publicació: ${response.data[index].data}</small>
          </div>
        </div>
       </div>
      </div>
      `
  }
});