document.getElementById("btnUser").addEventListener('click', function() {
    borrarUsuari();
});

function borrarUsuari() {
    let usuari = document.getElementsByName('userDel')[0].value;

    let params = new URLSearchParams();
    params.append('usuari', usuari);
  
    axios.post('borrarUsuari.php', params).then(function(response) {
      console.log(response.data);
      if(response.data == "¡Operació realitzada correctament!") {
        Swal.fire({
          icon: 'success',
          title: 'Usuari esborrat',
        })
      } else {
        Swal.fire({
          icon: 'error',
          title: 'ERROR',
        })
      }
    })
    .catch(function (error) {
      console.log(error.response);
    });
  }