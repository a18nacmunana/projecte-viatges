function punctuarPosExp(codi) {
    let params = new URLSearchParams();
    params.append('codiExp', codi);
    
    axios.post('punctuarExpPos.php', params).then(function(response) {
        if(response.data == "¡Operación realizada correctamente!") {
            $.amaran({
                content:{
                    title:'Has votat positivament!',
                    message:'',
                    info:'',
                    icon:'fa fa-check'
                },
                theme:'awesome ok'
            });
            let id_exp = document.getElementById("Pos"+codi);
            id_exp.innerHTML = parseInt(id_exp.innerHTML) + 1;
    
          } else {
            $.amaran({
                content:{
                    title:'Ja l\'has punctuat!',
                    message:'',
                    info:':(',
                    icon:'fa fa-error'
                },
                theme:'awesome error'
            });
          }
    });
}

function punctuarNegExp(codi) {
    let params = new URLSearchParams();
    params.append('codiExp', codi);
    
    axios.post('punctuarExpNeg.php', params).then(function(response) {
        if(response.data == "¡Operación realizada correctamente!") {
            $.amaran({
                content:{
                    title:'Has votat negativament!',
                    message:'',
                    info:'',
                    icon:'fa fa-minus-circle'
                },
                theme:'awesome error'
            });

            let id_exp = document.getElementById("Neg"+codi);
            id_exp.innerHTML = parseInt(id_exp.innerHTML) + 1;
          } else {
            $.amaran({
                content:{
                    title:'Ja l\'has punctuat!',
                    message:'',
                    info:':(',
                    icon:'fa fa-error'
                },
                theme:'awesome error'
            });
          }
    });
}
