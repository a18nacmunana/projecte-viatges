<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: text/html; charset=utf-8");

    require "viatges_bdd.php";
    require "configuracio_bdd.php";
    $array = array("funciona" => "no");
    if($_POST['categoria']=="Opcions..."){
        echo json_encode($array);
    }
    else{
        $bdd = new ViatgesBDD($db_host, $db_user, $db_pass, $db_name);
        $codi = $bdd->getCodiExperienciesByName($_POST['categoria']);
        $codi = $codi[0];
        $codi = $codi['codi'];
        echo json_encode(mb_convert_encoding($bdd->getExperienciesByCategoria($codi), "UTF-8"));
    }
?>